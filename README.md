## 💻 Sobre o projeto

Uma aplicação web que ajude o usuário a calcular a quantidade de tinta necessária para pintar uma sala.
Essa aplicação considera que a sala é composta de 4 paredes e permite que o usuário escolha qual a medida de cada parede e quantas janelas e portas possuem cada parede.
Com base na quantidade necessária o sistema aponta tamanhos de lata de tinta que o usuário deve comprar, sempre priorizando as latas maiores. Ex: se o usuário precisa de 19 litros, ele deve sugerir 1 lata de 18L + 2 latas de 0,5L

Regras de negócio

Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 50 metros quadrados, mas podem possuir alturas e larguras diferentes
O total de área das portas e janelas deve ser no máximo 50% da área de parede
A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta
Cada janela possui as medidas: 2,00 x 1,20 mtos
Cada porta possui as medidas: 0,80 x 1,90
Cada litro de tinta é capaz de pintar 5 metros quadrados
Não considerar teto nem piso.
As variações de tamanho das latas de tinta são:

0,5 L
2,5 L
3,6 L
18 L

🛠 Tecnologias

As seguintes ferramentas foram usadas na construção do projeto:

* NextJS
* Styled Components
* ANTD Designer


## 🚀 Como executar o projeto

### Pré-requisitos

- Para rodar localmente você precisará ter o node  instalado.

Comandos para rodar local:
```bash
# Clone este repositório
$ git clone git@gitlab.com:joaooliveira966/paint_calculator.git

# instale as dendências
$ npm install

# execute o build
$ npm run build

# start na aplicação
$ npm start

```


### DOCKER
* Para rodar em um container virtual você precisará ter o Docker instalado.

Comandos para executar virtual:
```bash
# instala as dependencias inicia a aplicação
$ npm run docker
```

Feito com ❤️ por João Guilherme de Oliveira!
